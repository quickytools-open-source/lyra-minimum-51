// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "LyraSourceOnlyGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class LYRASOURCEONLY_API ALyraSourceOnlyGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
